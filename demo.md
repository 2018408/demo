run_tests:
  before_script:
    - echo "Preparing test data..."
  script:
    - echo "Runing tests..."
    after_script:
     - echo "Clean up temporary files..."

build_image:
  script:
    - echo "build the docker image"
    - echo "Tagging the docker image"

push_image:
  script:
    - echo "Logging info  docker registry"
    - echo "Pushing docker image to registry"